#include <stdio.h>
#include <stdbool.h>


int main()
{
        int triangle = 0;  // current triangle number
        int nthNum = 1;  // used to calculate next
        int factors;
        int divisor;  // used as divisor for determination of divisibility
        int result = 0;  // result of division
        bool enoughFactors = false;  // becomes true when 500 factors are observed

        while(!enoughFactors) {
                triangle += nthNum;
                nthNum++;
                factors = 0;
                divisor = 1;
                result = triangle;

                //  this logic is borrowed from problem 10, documentation is there
                while(result > divisor) {
                        if(triangle % divisor == 0)
                                factors += 2;
                        result = triangle / divisor;
                        divisor += 1;
                }

                if(factors > 500)
                        enoughFactors = true;
        }


        printf("%d", triangle);

        return 0;
}