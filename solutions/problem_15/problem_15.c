#include <stdio.h>

int main()
{
    double grid[21][21] = {0};  // counting sides, not intersections, so the size is 21x21

    // sets the bottom and right edge of the grid to 1, only one path exists from these positions
    for(int i = 0; i < 21; i++) {
        grid[20][i] = 1;
        grid[i][20] = 1;
    }

    // sum of the paths to the right and down gives paths from current intersection
    for(int x = 19; x >= 0; x--) {
        for(int y = 19; y >= 0; y--) {
            grid[x][y] = grid[x+1][y] + grid[x][y+1];
        }
    }

    // prints the table, not necessary but helpful
    /*
    for(int x = 0; x < 21; x++) {
        for (int y = 0; y < 21; y++)
            printf("%15.f", grid[x][y]);
        printf("\n");

    }
*/

    printf("%.f", grid[0][0]);
    
    
    return 0;
}