# Find the sum of all the positive integers which cannot be written as the sum
# of two abundant numbers.

require 'set'

def divisorSum(value)
    divisors = []
    sum = 0

    for x in 1..value-1
        if value % x == 0
            divisors.push(x)
        end
    end

    divisors.each do |item|
        sum += item
    end

    return sum
end


#  MAIN  #
abundants = []  # array of all abundants (below the threshold of relevance)
sums = Set.new  # array of all numbers which are a sum of two abundants

print("Calculating divisors...  ")
for x in 1..28123
    if divisorSum(x) > x
        abundants.push(x)
    end
end

for x in 0..(abundants.size - 1)
    for y in x..(abundants.size - 1)
        if !sums.include?(abundants[x] + abundants[y])
            sums.add(abundants[x] + abundants[y])
        end
    end
end

total = 0  # for the sum the question asks for
print("Done.\nCalculating sums...  ")

for x in 1..28123
    if !sums.include?(x)
        total += x
    end
end

print("Done.\n\n\nAnswer:", total, "\n")
