file = open("names.txt", "r")

names = file.read().split(',')

for i, name in enumerate(names):
	if(name[-1] == '\n'):
		names[i] = name[1:-2]
	else:
		names[i] = name[1:-1]

names.sort()

name_scores = [0] * len(names)
sum = 0

for i, name in enumerate(names):
	for c in name:
		name_scores[i] += ord(c) - 64

for i in range(0, len(names)):
	sum += name_scores[i] * (i + 1)

print(sum)