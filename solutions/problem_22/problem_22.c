#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void append(char* s, char c)
{
    int len = strlen(s);
    if(len >= 11) {
        // name is too large, if arbitrary length is needed then use malloc
        printf("\"%s%c\" is too long, this program is only fuctional up to",
            "length 10.\n", s, c);
        exit(1);
    }

    s[len] = c;
    s[len+1] = '\0';
}

// this must be the function signature for qsort's comparison
int comp(const void* x, const void* y)
{
    const char* a = *(const char**)x;  // string pointed to by x
    const char* b = *(const char**)y;
    
    return strcmp(a, b);
}


int main()
{
    char* namearr[5163];  // create an array with space for each name
    
    for(int x = 0; x < 5163; x++) {
        // each name gets default size 11  NOTE: memory for time trade
        namearr[x] = malloc(12);
        namearr[x][0] = '\0';
    }

    FILE* file;
    file = fopen("names.txt", "r");

    int x = 0;  // incrementor
    char next;
    

    while((next = fgetc(file)) != EOF) {
        if(next != '\"' && next != '\n') {
            if(next == ',') {
                x++;
            }

            else
                append(namearr[x], next);
        }
    }
    
    fclose(file); 
    // sort the array alphabetically
    qsort(namearr, 5163, sizeof(char*), comp);

    int scores[5163];
    for(int x = 0; x < 5163; x++) {
        for(int y = 0; y < strlen(namearr[x]); y++) {
            // assign each in scores the sum of the letter values
            scores[x] += namearr[x][y] - 64;
        }

        scores[x] *= x+1;  // multiply by its position in the list
    }

    int sum = 0;
    for(int x = 0; x < 5163; x++)
        sum += scores[x];

    printf("%d\n", sum);


    return 0;
}
