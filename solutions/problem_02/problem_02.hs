-- Infinite list of fibonacci numbers
fibonaccis = 1 : 2 : zipWith (+) fibonaccis (tail fibonaccis)


-- Solution
main = do print(sum [x | x <- takeWhile (< 4000000) fibonaccis, x `mod` 2 == 0])