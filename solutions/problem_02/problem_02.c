#include <stdio.h>

int main()
{
  int last = 1;
  int lastlast = 0;
  int temp;

  int sum = 0;
  
  while(last+lastlast < 4000000) {
    if((last + lastlast) % 2 == 0) {
      sum += last + lastlast;
    }
    
    printf("%d\n", last+lastlast);

    temp = last;
    last = last + lastlast;
    lastlast = temp;

  }

  printf("\n%d\n", sum);
}
