divisByEach :: Int -> [Int] -> Bool
divisByEach x ds = if filter (/= 0) (map (mod x) ds) == [] then True
                   else False


main = do print(head [x | x <- [20, 40..], x `mod` 20 == 0, divisByEach x [1..20]])
