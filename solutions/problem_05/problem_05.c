#include <stdio.h>
#include <stdbool.h>

int main()
{
        int current = 0;
        bool found = false;

        while(!found) {
                found = true;
                current += 20;

                for(int x = 2; x <= 20; x++)
                        if(current % x != 0)
                                found = false;
        }

        printf("%d", current);

        return 0;
}
