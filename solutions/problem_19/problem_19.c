#include <stdio.h>

int main()
{
        int months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int first_day = 2;  // 0 is sunday, 1 is monday, etc.
        int sundays = 0;

        for(int year = 1901; year < 2001; year++) {
                if(year % 4 == 0)
                        months[1] = 29;
                else
                        months[1] = 28;

                for(int x = 0; x < 12; x++) {
                        first_day += months[x] % 7;  // describes movement of the 1st
                        if(first_day >= 7)
                                first_day -= 7;  // keep between 0-6

                        if(first_day == 0)
                                sundays++;
                }
        }

        printf("%d", sundays);


        return 0;
}