#include <stdio.h>

int main()
{
    int digits[305] = {0};  // 2^1000 has just over 300 digits
    digits[304] = 2;  // set initial value
    int carry;  // used for carry in arithmetic
    int sum = 0;  // answer

    for(int x = 1; x < 1000; x++) {
        carry = 0;

        for(int i = 304; i >= 0; i--) {
            carry += digits[i] * 2;
            digits[i] = carry % 10;
            carry -= digits[i];
            carry /= 10;
        }
    }

    for(int x = 0; x < 305; x++)
        sum += digits[x];

    printf("%d", sum);


    return 0;
}