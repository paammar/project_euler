#  What is the largest prime factor of the number 600851475143?
import math


def factors(n):
    result = []

    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            result.append(i)
            if i != math.sqrt(n):
                result.append(int(n / i))

    return result


def is_prime(n):
    if factors(n) == [1, n]:
        return True
    else:
        return False


def main():
    n = 600851475143
    n_facts = factors(600851475143)
    result = 0

    for fact in n_facts:
        if is_prime(fact):
            result = fact

    print(result)


if __name__ == "__main__":
    main()