-- NOTE: Is incorrect for input 1, either account for this or fix it
isPrime :: Int -> Bool
isPrime x = if [i | i <- [2..(x-1)], x `mod` i == 0] == [] then True
            else False


-- NOTE: may have duplicates
factors :: Int -> [Int]
factors x = concat [[i, x `div` i]
            | i <- [1..(floor . sqrt . fromIntegral) x + 1], x `mod` i == 0]


main = do print(last [x | x <- factors 600851475143, isPrime x])