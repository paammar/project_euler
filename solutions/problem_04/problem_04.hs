-- Accepts an integer and returns a list of its digits
digits :: Int -> [Int]
digits 0 = []
digits x = digits (x `div` 10) ++ [x `mod` 10]


-- Boolean for palindromic numbers
palindromic :: Int -> Bool
palindromic x = if digits x == reverse (digits x) then True
                else False

main = do print(maximum [(x * y) |
                        x <- [100..999],
                        y <- [100..999],
                        palindromic (x * y)])