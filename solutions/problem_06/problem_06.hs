s = sum [x ^ 2 | x <- [1..100]]


main = do print((sum [1..100]) ^ 2 - s)