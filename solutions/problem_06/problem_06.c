#include <stdio.h>

int main()
{
        int sumSquares = 0;
        int squareSum = 0;

        for(int x = 1; x <= 100; x++) {
                sumSquares += x * x;
                squareSum += x;
        }

        squareSum = squareSum * squareSum;

        printf("%d", squareSum-sumSquares);


        return 0;
}
