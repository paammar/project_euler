count = 1
current = 3

while count < 10001:
    
    foundFactor = False

    for x in range(current//2, 2, -1):
        if current % x == 0:
            foundFactor = True
            break

    if not foundFactor:
            count += 1

    current += 2
