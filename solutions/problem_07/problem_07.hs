-- NOTE: May have duplicates
factors :: Int -> [Int]
factors x = concat [[i, x `div` i]
            | i <- [1..(floor . sqrt . fromIntegral) x + 1], x `mod` i == 0]


-- NOTE: Is incorrect for input 1, either account for this or fix it
isPrime :: Int -> Bool
isPrime x = if filter (\y -> y /= 1 && y /= x) (factors x) == [] then True
            else False


main = do print([x | x <- 2:[3,5..], isPrime x] !! 10000)