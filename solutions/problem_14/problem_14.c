#include <stdio.h>
#include <math.h>

int main() {
    int longest = 0;
    int longest_seed = 0;
    long current;
    int current_length;

    for (int x = 2; x < 1000000; x++) {
        current = x;
        current_length = 2;  // accounting for first and last term, which loop does not count

        while (current > 1) {
            if (current % 2 == 0)
                current /= 2;
            else
                current = current * 3 + 1;

            current_length++;
        }

        if (current_length > longest) {
            longest = current_length;
            longest_seed = x;
        }
    }

    printf("\n%d", longest_seed);


    return 0;
}