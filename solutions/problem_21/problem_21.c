#include <stdio.h>

int divisorSum(int n)
{
        int sum = 1;  // start with 1 because all divisible by 1

        for(int x = 2; x < n; x++) {
                if(n % x == 0)
                        sum += x;
        }

        return sum;
}

int main()
{
        int amicableSum = 0;  // answer

        for(int x = 2; x < 10000; x++)
                if(divisorSum(x) != x && divisorSum(divisorSum(x)) == x)
                        amicableSum += x;

        printf("%d", amicableSum);


        return 0;
}