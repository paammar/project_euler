sumPrimes = 2  # starting at 1 to account for prime 2, which is skipped in the loop

for current in range(3, 1999999, 2):  # starting with 3 to skip all even numbers, as none but 2 are prime
    isPrime = True  # prime until proven divisible
    divisor = 3  # skip 2, no evens
    result = 4  # arbitrary starting value that will enter loop

    while result > divisor:  # if result > divisor, no factors can exist above current divisor (see end of file)
        if current % divisor == 0:
            isPrime = False
            break
        result = current / divisor
        divisor += 1

    if isPrime:
        sumPrimes += current
        print("PRIME: ", current)

print(sumPrimes)


### Explanation
#
# In order to make prime checking more efficient, this algorithm continues to check for factors upwards
# until the divisor (first candidate factor) is greater than the result of the division
# (second candidate factor). Once the divisor becomes greater than the result, continuing to search 
# for factors above the current divisor becomes redundant. If any factors existed above the current
# divisor, their partner would have to exist below it, but these values have already been checked, 
# so there cannot be any.
