#include <stdio.h>

int main()
{
        int digits[160] = {0};  // has roughly 160 digits
        digits[159] = 1;  // set initial value
        int carry;  // used for arithmetic
        int sum = 0;  // answer

        for(int x = 2; x <= 100; x++) {
                carry = 0;

                for(int i = 159; i >= 0; i--) {
                        carry += digits[i] * x;
                        digits[i] = carry % 10;
                        carry -= digits[i];
                        carry /= 10;
                }
        }

        for(int x = 0; x < 160; x++)
                sum += digits[x];

        printf("\n%d", sum);


        return 0;
}