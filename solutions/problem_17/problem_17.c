#include <stdio.h>

int main()
{
    // arrays for number of letters in the ones, tens, and teens
    int ones[10] = {0, 3, 3, 5, 4, 4, 3, 5, 5, 4};
    int teens[10] = {3, 6, 6, 8, 8, 7, 7, 9, 8, 8};
    int tens[10] = {0, 0, 6, 6, 5, 5, 5, 7, 6, 6};
    int total = 0;

    for(int x = 1; x <= 1000; x++) {
        if(x % 100 >= 10 && x % 100 < 20)
            total += teens[x % 10];
        else {
            total += ones[x % 10];
            total += tens[(x % 100 - x % 10) / 10];
        }

        if(x >= 100 && x < 1000) {
            total += ones[(x - x % 100) / 100] + 10;

            if(x % 100 == 0)
                total -= 3;  // no "and" for even hundreds
        }

        if(x == 1000)
            total += 11;  // 1000 falls through everything else
    }

    printf("\n%d", total);


    return 0;
}