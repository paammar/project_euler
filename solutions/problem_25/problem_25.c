#include <stdio.h>

void add(int* arr1, int* arr2, int index)
{ 	// this function adds arr1 to arr2
	if(index == 999)
		return;  // base case of recursion

	int sum = arr1[index] + arr2[index];
	arr2[index] = sum % 10;  // add ones place first
	sum -= sum % 10;
	sum /= 10;
	arr2[index + 1] += sum;  // add tens place

	add(arr1, arr2, index + 1);
}

int main()
{
	int f1[1000] = {0};
	int f2[1000] = {0};

	// initial conditions
	f1[0] = 1;
	f2[0] = 1;
	int index = 2;

	while(f1[999] == 0 && f2[999] == 0) {
		// oscillate between adding f1 to f2 and f2 to f1 based on index
		if(index % 2 == 0)
			add(f1, f2, 0);
		else
			add(f2, f1, 0);

		index++;
	}

	printf("%d\n", index);


	return 0;
}