# **WARNING: Spoilers ahead!**
**This repository contains spoilers for Project Euler problems. If you want to
solve them yourself, turn back.**

## Project Euler
These are my solutions to Project Euler problems, and serves as a version
control repo for those I'm currently working on. I'm going through the archive
of problems in a quasi-sequential order, as time permits.

### License
The software is licensed under the MIT license. For details see
[LICENSE](https://gitlab.com/paammar/project_euler/blob/master/LICENSE).
